import io.smallrye.mutiny.Multi;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MyExampleProcessBean {

    @Outgoing("process-a")
    public Multi<String> procuder(){
        return Multi.createFrom().items("message","from","process","a");
    }

    @Incoming("process-a")
    @Outgoing("process-b")
    public String process(String inMessage){
        return String.format("Process send message : %s",inMessage);
    }

    @Incoming("process-b")
    public void consumer(String message){
        System.out.println(message);
    }
}